

<!DOCTYPE html>

<html>

<head>
<meta charset="utf-8">
<title>Upload Page</title>
</head>

<?php
session_start();

$username = $_SESSION['username'];
$path = $_SESSION['path'];

?>
<body>
<form enctype="multipart/form-data" action = "upload.php" method="POST">
	<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
	Upload this file: <input name="uploadedfile" type="file" /> <br>
	Destination folder:
	<select name="myDirs">
	<option value="" selected="selected">Select a folder</option>
	<?php

	foreach ( new DirectoryIterator("/srv/uploads/".$username) as $fileInfo) {
    	if($fileInfo->isDir() && !$fileInfo->isDot()) {
		echo '<option value="'.$fileInfo->getFileName().'">'.$fileInfo->getFileName()."</option>\n";
    	}
	}
	?>
	</select>
	<br>
	<input type="submit" value="Upload File" /> <br>
	
	
	
</form>

    <ul>
    	<li><a href="home.php">Back to home page</a></li>
	</ul>


</body>

</html>