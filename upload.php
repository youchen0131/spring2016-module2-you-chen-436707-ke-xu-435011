<?php
session_start();

$username = $_SESSION['username'];
$path = $_SESSION['path'];
// Get the filename and make sure it is valid
$filename = basename($_FILES['uploadedfile']['name']);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}

$full_path = sprintf("/srv/uploads".'/'.$username.'/'.$_POST['myDirs'].'/'.$filename);


if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
	echo 'Upload suceeded';
	header("Location: files.php");
	exit;
}else{
	echo 'Upload failed';
	exit;
}

 
?>
