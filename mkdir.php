<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Create a folder</title>
</head>

<?php
	session_start();
    $username = $_SESSION['username'];

		
	
if (isset($_POST['dirname'])) {
	$newdir=$_POST['dirname'];
	if ( mkdir('/srv/uploads/'.$username.'/'.$newdir, 775) ){
		$old = umask(0);
		chmod('/srv/uploads/'.$username.'/'.$newdir, 0775);
		umask($old);
		if ($old != umask()){
			echo "error occured while changing back the umask";
		}
	
		echo "folder ".$newdir. " successfully created";
	} else {
		echo "Invalid folder name, try again";
	}
}

?>

<body>
	<form action="mkdir.php" method="POST">
		<label for="dirname">Enter the name for the new folder</label>
		<input type="text" name="dirname" id="dirname">
		<input type="submit" value="create folder">
	</form>
    <ul>
    	<li><a href="files.php">Back to directory</a></li>
	</ul>
</body>
</html>


